uniform float viewDistance;

struct VS_OUTPUT
{
	float4 position 	: POSITION;
	float2 texcoord0	: TEXCOORD0;
	float3 normal		: TEXCOORD1;
	float3 tangent		: TEXCOORD2;
	float3 bitangent	: TEXCOORD3;
	float  depth		: TEXCOORD4;
	float4 color	 	: COLOR0;
};

struct PS_OUTPUT
{
	float4 color0 : COLOR0; // Diffuse, Emission factor
	float4 color1 : COLOR1; // Normals, Ns
	float4 color2 : COLOR2; // Depth
};

sampler2D textureDiffuseSampler;
sampler2D textureNormalSampler;

PS_OUTPUT main( VS_OUTPUT IN )
{
	PS_OUTPUT OUT;
	
	float3x3 tangentToSpace = transpose(float3x3(IN.tangent, IN.bitangent, IN.normal));
	
	OUT.color0 = float4(tex2D(textureDiffuseSampler, IN.texcoord0).xyz, IN.color.x);
	OUT.color1 = float4((mul(tangentToSpace, tex2D(textureNormalSampler, IN.texcoord0).xyz * 2.0f - 1.0f) + 1.0f) / 2.0f, IN.color.w);
	OUT.color2 = IN.depth / viewDistance;

	return OUT;
}
