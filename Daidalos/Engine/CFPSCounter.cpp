/*
==============================================================================================================================================================

Author: Stephane Baudoux
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../Managers/CLogManager.h"
#include "../Managers/COptionManager.h"
#include "../Misc/Global.h"
#include "../Engine/Entities/2D/CFontEntity.h"
#include "../Engine/CFPSCounter.h"

/**
* Constructor
*/
CFPSCounter::CFPSCounter()
{
	Initialize();
}

/**
* Destructor
*/
CFPSCounter::~CFPSCounter()
{
	if(this->m_executionTime > 0 && this->m_optionM->IsDebugModeEnabled(OPTIONMANAGER_CONFIGURATION_ACTIVE))
	{
		this->m_logM->GetStream() << "Average In-Game FPS: " << (this->m_frameCount / (this->m_executionTime / 1000.0f)) << "\n";
		this->m_logM->Write();
	}
	SAFE_DELETE(this->m_font);
}

/**
* Initialize the manager
*/
void CFPSCounter::Initialize()
{
	this->m_logM = CLogManager::GetInstance();
	this->m_optionM = COptionManager::GetInstance();

	std::ostringstream stream;
	stream.str("");
	stream << "Fonts/" << this->m_optionM->GetTexturePath(OPTIONMANAGER_CONFIGURATION_ACTIVE) << "/" << "default.tga";

	Reset();
	
	this->m_fpsStream << "FPS: 0";
	this->m_font = new CFontEntity(stream.str(), " ", 0.75f, FONT_ALIGN_LEFT, (int)(this->m_optionM->GetWidth(OPTIONMANAGER_CONFIGURATION_ACTIVE) * FPS_TEXT_X), (int)(this->m_optionM->GetHeight(OPTIONMANAGER_CONFIGURATION_ACTIVE) * FPS_TEXT_Y), 0xFFFFFF00);
}

/**
* Update the manager
* @param time Elaped time
*/
void CFPSCounter::Update(const float time)
{
	this->m_frameCount++;
	this->m_frameCountRate++;
	this->m_executionTime += time;
	this->m_fpsUpdateRateTimer += time;
	if(this->m_fpsUpdateRateTimer >= FPS_UPDATE_RATE)
	{
		this->m_fps = ((int)((this->m_frameCountRate / (this->m_fpsUpdateRateTimer / 1000.0f)) * 100)) / 100.0f;
		this->m_fpsUpdateRateTimer = 0;
		this->m_frameCountRate = 0;
		this->m_fpsStream.str("");
		this->m_fpsStream << "FPS: " << this->m_fps;
		this->m_font->SetText(this->m_fpsStream.str());
	}
}

/**
* Reset the fps counter
*/
void CFPSCounter::Reset()
{
	this->m_frameCount = 0;
	this->m_frameCountRate = 0;
	this->m_executionTime = 0;
	this->m_fpsUpdateRateTimer = 0;
}

/**
* Render the fps
*/
void CFPSCounter::Render()
{
	this->m_font->Render();
}

/**
* Get the current fps
* @return The current fps
*/
const float CFPSCounter::GetFPS() const
{
	return this->m_fps;
}
